package avengers.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import avengers.model.TaskList;

/**
 * Class to manipulate the task.json using CRUD. This contains methods to add,
 * create, delete and update lists and tasks
 * 
 * @author Matheus André Pereira Góes
 * @author Victor Gabriel Alves Pereira
 * @author Victor Matheus Carvalho Pinheiro
 * @author Wesley Claudino Rodrigues
 * @version 1.0.0
 */
public class TaskJSON {
  private File file;
  private Gson gson = new Gson();
  private List<TaskList> list = new LinkedList<TaskList>();

  /**
   * Constructor to load (or create if needed) the task.json and load data into
   * List<TaskList>
   */
  public TaskJSON() {
    try {
      if (!new File("data").exists())
        new File("data").mkdir();

      file = new File("data", "task.json");
      file.createNewFile();
      getJson();
    } catch (IOException e) {
      System.out.println("Exception: " + e.getMessage());
    }
  }

  /**
   * Load data from file into list if newList is not null
   * 
   * @throws JsonSyntaxException   when JSON is malformed
   * @throws JsonIOException       when it's not able to read or write to JSON
   * @throws FileNotFoundException when file is not found
   */
  public void getJson() throws JsonSyntaxException, JsonIOException, FileNotFoundException {
    Type listType = new TypeToken<List<TaskList>>() {
    }.getType();
    List<TaskList> newList = gson.fromJson(new FileReader(file), listType);

    if (newList != null) {
      list = newList;
    }
  }

  /**
   * Write the all changes of List<TaskList> in task.json
   */
  public void writeData() {
    try {
      Writer writer = new FileWriter(file);
      gson.toJson(list, writer);
      writer.close();
    } catch (IOException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Get the id of last TaskList and return it. If there is no element, return 0
   * 
   * @param tmp TaskList to get ID
   * @return id of last TaskList
   */
  public int getLastId(List<TaskList> tmp) {
    if (list.size() <= 0)
      return 0;

    int size = list.size() - 1;
    TaskList lastElement = tmp.get(size);
    return lastElement.getId();
  }

  /**
   * Add a new list to task.json using the size of list as id
   * 
   * @param name name of list of tasks
   */
  public void createList(String name) {
    try {
      int id = getLastId(list) + 1;

      TaskList taskList = new TaskList(name, id);
      list.add(taskList);
    } catch (JsonSyntaxException | JsonIOException e) {
      System.out.println("Error: " + e.getMessage());
    } catch (IndexOutOfBoundsException e) {
      System.out.println("Error: " + e.getMessage());
    }

    writeData();
  }

  /**
   * Update the properties of a TaskList if exists.
   * 
   * @param newTaskList values to insert into TaskList if exists.
   */
  public void updatePropertiesList(TaskList newTaskList) {
    for (TaskList taskList : list) {
      if (taskList.getId() == newTaskList.getId()) {
        taskList.setName(newTaskList.getName());
        taskList.setTasks((newTaskList.getTasks()));
        taskList.setUpdatedAt(LocalDateTime.now().toString());
      }
    }
    writeData();
  }

  /**
   * Delete the TaskList from list
   * 
   * @param id id of TaskList to be removed
   */
  public void deleteList(int id) {
    TaskList tmp = null;
    for (TaskList taskList : list) {
      if (taskList.getId() == id) {
        tmp = taskList;
      }
    }

    list.remove(tmp);
    writeData();
  }

  /**
   * Search for a TaskList using id and return it if exists
   * 
   * @param id of TaskList
   * @return TaskList with same id. If don't exist, return null
   */
  public TaskList getTaskList(int id) {
    for (TaskList taskList : list) {
      if (taskList.getId() == id) {
        return taskList;
      }
    }

    return null;
  }
}
