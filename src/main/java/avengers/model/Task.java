package avengers.model;

import com.google.gson.annotations.SerializedName;

public class Task extends SuperList {
	@SerializedName("list-id")
	private int listId;
	@SerializedName("description")
	private String description;

	public Task(String name, String description) {
		super(name);
		this.description = description;
	}

	public int getListId() {
		return listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}