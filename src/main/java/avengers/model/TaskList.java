package avengers.model;

import java.util.LinkedList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TaskList extends SuperList {
	@SerializedName("tasks")
	private List<Task> tasks = new LinkedList<Task>();

	public TaskList(String name, int id) {
		super(name, id);
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Task task) {
		this.tasks.add(task);
	}

	public void setTasks(List<Task> list) {
		this.tasks = list;
	}
}
