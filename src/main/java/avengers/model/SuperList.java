package avengers.model;

import java.time.LocalDateTime;

import com.google.gson.annotations.SerializedName;

/**
 * Abstract class using Serialized attributes to persist data into task.json
 * 
 * @author Matheus André Pereira Góes
 * @author Victor Gabriel Alves Pereira
 * @author Victor Matheus Carvalho Pinheiro
 * @author Wesley Claudino Rodrigues
 * @version 1.0.0
 */
public abstract class SuperList {
	@SerializedName("id")
	private int id;
	@SerializedName("name")
	private String name;
	@SerializedName("created-at")
	private String createdAt;
	@SerializedName("updated-at")
	private String updatedAt;

	/**
	 * Assign values from params into current object
	 * 
	 * @param name name of task or list
	 * @param id   id of task or list
	 */
	public SuperList(String name, int id) {
		this.id = id;
		this.name = name;
		this.createdAt = LocalDateTime.now().toString();
		this.updatedAt = LocalDateTime.now().toString();
	}

	/**
	 * Get the value of id
	 * 
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get the value of name
	 * 
	 * @return name of task or list
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the value from param into name
	 * 
	 * @param name name of task or list
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the value of createdAt
	 * 
	 * @return date (createdAt) of task or list
	 */
	public String getCreatedAt() {
		return createdAt;
	}

	/**
	 * Get the value of updateAt
	 * 
	 * @return date (createdAt) of task or list
	 */
	public String getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * Set the value of param into updateAt
	 * 
	 * @param updatedAt date (updatedAt) of task or list
	 */
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
